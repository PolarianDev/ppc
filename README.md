# Polarian Project Creator (PPC)
A simple command line utility to create default projects, this is useful for languages such as C which do not offer a default project structure, furthermore it allows me to adapt structures to how I like it and make a new project quickly when needed
