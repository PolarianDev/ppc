#include <stdio.h>
#include <string.h>

int genCProj()
{
    char* debugFile = "#!/bin/bash\n\
echo -e '**** Compiling code ****'\n\
cmake -S . -B build/debug -D CMAKE_C_COMPILER=gcc\n\
make -C build/debug\n\n\
echo -e '**** Executing code ****'\n\
cd build/debug\n";
    FILE *f = fopen("debug", "w");
    if (f == NULL)
    {
        puts("Failed to write to file\n");
        return 1;
    }
    fputs(debugFile, f);
    fclose(f);
    return 0;
}

int main(int argc, char* argv[])
{
    int r = genCProj();
    switch (r)
    {
        case 0:
            puts("Successfully created new project");
            break;
        case 1:
            puts("Failed to create new project");
            break;
    }
    return 0;
}

